#ifndef HYLMOR_SIMULATION_H
#define HYLMOR_SIMULATION_H

#include <iostream>
#include <string>

#include "drake/systems/framework/diagram.h"
#include "drake/systems/framework/diagram_builder.h"
#include "drake/multibody/rigid_body_tree_construction.h"
#include "drake/multibody/parsers/urdf_parser.h"
#include "drake/multibody/rigid_body_plant/rigid_body_plant.h"
#include "drake/lcm/drake_lcm.h"
#include "drake/multibody/rigid_body_plant/contact_results_to_lcm.h"
#include "drake/multibody/rigid_body_plant/drake_visualizer.h"

#include "drake/systems/primitives/constant_vector_source.h"

namespace drake {
namespace hylmor {


class HylmorSimulationDiagram : public systems::Diagram<double> {
public:
	HylmorSimulationDiagram(lcm::DrakeLcm* lcm);
	systems::RigidBodyPlant<double>* get_mutable_plant();
private:
	systems::RigidBodyPlant<double>* plant_;


};

}  // namespace hylmor
}  // namespace drake

#endif // HYLMOR_SIMULATION_H
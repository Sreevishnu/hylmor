
#include "drake/systems/framework/leaf_system.h"
#include "drake/systems/rendering/frame_velocity.h"
#include "drake/systems/rendering/pose_vector.h"


namespace drake {
namespace hylmor {

template <typename T>
class Hylmor : public systems::LeafSystem<T> {
public:

  Hylmor();

  void DoCalcTimeDerivatives(
    const systems::Context<T>& context,
    systems::ContinuousState<T>* derivatives) const override;

  const systems::OutputPort<T>& state_output() const;
  const systems::OutputPort<T>& pose_output() const;
  const systems::OutputPort<T>& velocity_output() const;
};
} //hylmor
} //drake

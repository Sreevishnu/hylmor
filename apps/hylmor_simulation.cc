
#include "apps/hylmor_simulation.h"
namespace drake {
namespace hylmor {

HylmorSimulationDiagram::HylmorSimulationDiagram(lcm::DrakeLcm* lcm){
	drake::systems::DiagramBuilder<double> builder;

	// Create RigidBodyTree.
    auto tree_ptr = std::make_unique<RigidBodyTree<double>>();

    drake::parsers::urdf::AddModelInstanceFromUrdfFile(
            "/home/sree/Research/HyLMoR/dev/hylmor/apps/models/hylmor_description/urdf/"           
            "hylmor_description_test.urdf",
        multibody::joints::kRollPitchYaw, nullptr /* weld to frame */,
        tree_ptr.get());

    multibody::AddFlatTerrainToWorld(tree_ptr.get(), 100.0, 10.0);
    
    // Instantiate a RigidBodyPlant from the RigidBodyTree.
    plant_ = builder.AddSystem<systems::RigidBodyPlant<double>>(
        std::move(tree_ptr));
    plant_->set_name("plant");

    const auto& tree = plant_->get_rigid_body_tree();

    // RigidBodyActuators.
    std::vector<const RigidBodyActuator*> actuators;
    for (const auto& actuator : tree.actuators) {
      actuators.push_back(&actuator);
    }

    VectorX<double> x0 = VectorX<double>::Zero(4);

    x0(0)=-.1;
    x0(1)=.1;
    x0(2)=-.1;
    x0(3)=.1;

    auto constant_zero_source =
     builder.AddSystem<systems::ConstantVectorSource<double>>(x0);
    builder.Connect(constant_zero_source->get_output_port(),
                 plant_->actuator_command_input_port());

    // Contact parameters
    const double kYoungsModulus = 1e8;  // Pa
    const double kDissipation = 5.0;  // s/m
    const double kStaticFriction = 0.9;
    const double kDynamicFriction = 0.5;
    systems::CompliantMaterial default_material;
    default_material.set_youngs_modulus(kYoungsModulus)
        .set_dissipation(kDissipation)
        .set_friction(kStaticFriction, kDynamicFriction);
    plant_->set_default_compliant_material(default_material);

    const double kStictionSlipTolerance = 0.01;  // m/s
    const double kContactRadius = 2e-3;  // m
    systems::CompliantContactModelParameters model_parameters;
    model_parameters.characteristic_radius = kContactRadius;
    model_parameters.v_stiction_tolerance = kStictionSlipTolerance;
    plant_->set_contact_model_parameters(model_parameters);

        // Visualizer.
    systems::DrakeVisualizer& visualizer_publisher =
        *builder.template AddSystem<systems::DrakeVisualizer>(tree, lcm);
    visualizer_publisher.set_name("visualizer_publisher");

    visualizer_publisher.set_publish_period(0.0005);
    // Raw state vector to visualizer.
    builder.Connect(plant_->state_output_port(),
                    visualizer_publisher.get_input_port(0));


    builder.BuildInto(this);
        
}

systems::RigidBodyPlant<double>* HylmorSimulationDiagram::get_mutable_plant() { 
	return plant_; 
}



}  // namespace hylmor
}  // namespace drake


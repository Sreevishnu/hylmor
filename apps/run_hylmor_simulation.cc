#include "apps/hylmor_simulation.h"
#include "drake/systems/analysis/simulator.h"
#include "drake/systems/analysis/semi_explicit_euler_integrator.h"

#include <iostream>
#include <fstream>
#include <gflags/gflags.h>



namespace drake {
namespace hylmor {

DEFINE_double(simulation_sec, 12, 
	"Number of seconds to simulate.");

DEFINE_string(simulation_type, "compliant",
              "Type of simulation, valid values are "
              "'timestepping','compliant'");

int DoMain(){

	DRAKE_DEMAND(FLAGS_simulation_sec > 0);
	// LCM communication.
  	lcm::DrakeLcm lcm;
	HylmorSimulationDiagram diagram(&lcm);

	// Create simulator.
  	systems::Simulator<double> simulator(diagram);

  	// Reset the integrator with parameters that support stable gripping, given
  	// the contact parameters.
  	systems::Context<double>& context = simulator.get_mutable_context();
  	const double max_step_size = 0.00005; //1e-4
  	simulator.reset_integrator<systems::SemiExplicitEulerIntegrator<double>>(
      diagram, max_step_size, &context);
  	
  	// To generate graphviz dot file
  /*  std::string dot;
  	dot = diagram.GetGraphvizString();
  	std::ofstream out("hylmor.dot");
  	out << dot;
  	out.close();*/

  	//simulator.set_publish_every_time_step(true);
  	simulator.StepTo(FLAGS_simulation_sec);	
	return 0;
}

}  // namespace hylmor
}  // namespace drake

int main(int argc, char* argv[]) {

  return drake::hylmor::DoMain();
}
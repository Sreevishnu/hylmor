#include <gtest/gtest.h>

#include <drake/common/find_resource.h>
#include <drake/multibody/parsers/urdf_parser.h>

namespace drake {
namespace hylmor {

GTEST_TEST(LoadHylmorSimplifiedTest, TestIfHylmorSimplifiedLoads) {
  auto tree_ = std::make_unique<RigidBodyTree<double>>();
  drake::parsers::urdf::AddModelInstanceFromUrdfFile(
      "/home/sree/Research/HyLMoR/dev/hylmor/apps/models/hylmor_description/urdf/"
      "hylmor_description.urdf",
      drake::multibody::joints::kFixed, nullptr, tree_.get());
  EXPECT_EQ(tree_->get_num_actuators(), 16);
  EXPECT_EQ(tree_->get_num_positions(), 19);
  EXPECT_EQ(tree_->get_bodies().size(), 21);
}

}  // namespace hylmor
}  // namespace drake
